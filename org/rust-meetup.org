* Rust meetup

* Rust uvod

** Rust kao jezik opće namjene

** Rust kao sistemski jezik

** Rust kao zamjena za C++


* Neka važna svojstva Rusta

** Model izvršavanja

** Borrow checker i lifetime

** Traits

** Algebarski tipovi

** Error handling

** Macros


* Elementi Rusta kroz case studije

** Referenca sa stacka

** Referenca u strukturi

** Data race

** Concurency


* Eko sistem

** Rustup

** Cargo

** Language server

** Wasm

** Dokumentacija

** Community


* Rust u TVbeatu
