#include <iostream>

using namespace std;

struct Foo {
  int x;
  int& y;
};

Foo make_foo() {
  int a = 1;
  int b = 2;
  return Foo { a, b };
}

int main() {
  auto foo = make_foo();
  cout << foo.y << endl;
}
