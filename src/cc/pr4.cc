#include <iostream>
#include <thread>

using namespace std;

thread run_thread() {
  int a = 0;
  return thread([&]() { for(int i = 0; i < 1000000; ++i) ++a; cout << a << endl; });
}

int main() {
  auto t = run_thread();
  t.join();
}
