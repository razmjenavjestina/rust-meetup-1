#include <iostream>
#include <thread>
#include <atomic>
using namespace std;

int main() {
  atomic<int> a(0);
  auto t1 = thread([&]() { for(int i = 0; i < 1000000; ++i) ++a; });
  auto t2 = thread([&]() { for(int i = 0; i < 1000000; ++i) ++a; });
  t1.join();
  t2.join();
  cout << a << endl;
}
