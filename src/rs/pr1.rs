fn foo() -> &u32 {
    let a = 5;
    &a
}

fn main() {
    println!("{}", foo());
}
