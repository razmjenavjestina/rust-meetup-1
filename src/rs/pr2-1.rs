struct Foo {
    x: u32,
    y: &u32,
}

fn make_foo() -> Foo {
    let a = 1;
    let b = 2;
    Foo { x: a, y: b }
}

fn main() {
    let foo = make_foo();
    println!("{}", foo.y);
}
