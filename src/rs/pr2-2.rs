struct Foo<'a> {
    x: u32,
    y: &'a u32,
}

fn make_foo() -> Foo<'static> {
    let a = 1;
    let b = 2;
    Foo { x: a, y: &b }
}

fn main() {
    let foo = make_foo();
    println!("{}", foo.y);
}
