struct Foo<'a> {
    x: u32,
    y: &'a u32,
}

fn make_foo<'a>(b: &'a u32) -> Foo<'a> {
    let a = 1;
    Foo { x: a, y: b }
}

fn main() {
    let b = 2;
    let foo = make_foo(&b);
    println!("{}", foo.y);
}
