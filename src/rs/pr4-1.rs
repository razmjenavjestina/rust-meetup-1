use std::thread;

fn run_thread() -> thread::JoinHandle<u32> {
    thread::spawn(|| 1)
}

fn main() {
    let ret = run_thread().join();
    println!("{}", ret.unwrap());
}
