use std::thread;

fn run_thread() -> thread::JoinHandle<u32> {
    let a = 0;
    let b = &mut a;
    thread::spawn(move || {for _i in 0 .. 1000000 { *b += 1} *b})
}

fn main() {
    let ret = run_thread().join();
    println!("{}", ret.unwrap());
}
