use std::sync::atomic::{AtomicUsize, Ordering};
use crossbeam;


fn main() {
    let mut cnt: AtomicUsize = AtomicUsize::new(0);
    crossbeam::scope(|scope| {
        let t1 = scope.spawn(|_| { for _ in 0 .. 1_000_000 {
            cnt.fetch_add(1, Ordering::SeqCst);
        }});
        let t2 = scope.spawn(|_| { for _ in 0 .. 1_000_000 {
            cnt.fetch_add(1, Ordering::SeqCst);
        }});
        //t1.join();
        //t2.join();
    });
    println!("{}", cnt.load(Ordering::SeqCst));
}

/*
fn main() {
    //let mut cnt: AtomicUsize = AtomicUsize::new(0);
    let mut cnt: usize = 0;
    crossbeam::scope(|scope| {
        let t1 = scope.spawn(|_| { for _ in 0 .. 1_000_000 {
            //cnt.fetch_add(1, Ordering::SeqCst);
            cnt += 1;
        }});
        let t2 = scope.spawn(|_| { for _ in 0 .. 1_000_000 {
            //cnt.fetch_add(1, Ordering::SeqCst);
            cnt += 1;
        }});
        //t1.join();
        //t2.join();
    });
    //println!("{}", cnt.load(Ordering::SeqCst));
    println!("{}", cnt);
}
*/
