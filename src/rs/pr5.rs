use std::thread;

fn main() {
    let x = 1;
    let y = &x;
    let t = thread::spawn(move || *y);
    let _r = t.join();
}
